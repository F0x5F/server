COMPOSE_PROJECT_NAME=bitrix-common
COMPOSE_PATH_SEPARATOR=:
# Сервисы, которые будем запускать
# Для включения Memcached добавить docker-compose/memcached.yml
# Для открытия порта MySQL (3306) после подключения mysql.yml добавить docker-compose/mysql.ports.yml
COMPOSE_FILE=docker-compose/docker-compose.yml:docker-compose/traefik.yml:docker-compose/grafana.yml:docker-compose/errors.yml:docker-compose/mysql.yml:docker-compose/mysql-adminer.yml:docker-compose/mailhog.yml:docker-compose/sphinx.yml
COMPOSE_HTTP_TIMEOUT=120
# Имя сети, если нужно сменить со значения по умолчанию (bitrix), то нужно также внести правки в ./docker-compose/docker-compose.yml в секции networks
COMPOSE_NETWORK_NAME=bitrix

# DOCKER_DEFAULT_PLATFORM=linux/amd64

# Имя основного хоста, на поддоменах которого будут открываться системные сервисы (traefik, adminer, mailhog)
MAIN_HOST=bitrix.local

# Timezone
SERVER_TIMEZONE=UTC

# Идентификатор пользователя и группы на хост-машине.
# В линукс-подобных системах и в терминале Windows можно получить командами: `id -u` и `id -g` соответственно.
# Чтобы не было проблемы с правами все операции с файлами сайта необходимо выполнять на хосте именно под этим пользователем.
# Не рекомендуется оставлять root (0:0), лучше завести отдельного пользователя.
UID=0
GID=0

### MySQL

# Образ контейнера сервера БД
# Старая версия 5.7, только для amd64
# MYSQL_IMAGE=percona
# Версия 8.0, архитектура amd64
# MYSQL_IMAGE=percona/percona-server:8.0.34-26.1
# Версия 8.0, архитектура arm64 (для процессоров Apple Silicon M1 / M2)
# MYSQL_IMAGE=percona/percona-server:8.0.34-26.1-aarch64
# Версия 8.0, универсальный образ для amd64 / arm64
# MYSQL_IMAGE=percona/percona-server:8.0.34-26.1-multi
# MariaDB
MYSQL_IMAGE=mariadb:10.11
# Имя файла конфига для БД. bitrix.cnf - для Percona, maria.cnf - для MariaDB
MYSQL_CONFIG_FILE_NAME=maria.cnf
# Путь к папке с конфигурационными файлами в контейнере db. /etc/my.cnf.d - для Percona, /etc/mysql/conf.d - для MariaDB
MYSQL_CONFIG_CONTAINER_PATH=/etc/mysql/conf.d

# Пароль пользователя MySQL - обязательно сменить на бою!!!
MYSQL_ROOT_PASSWORD=test

### Memcached
MEMCACHED_CONN_LIMIT=1024
MEMCACHED_MEMORY_LIMIT=1024
MEMCACHED_THREADS=8

### SSL & Redirects

# Резолвер для сертификатов, варианты: letsEncrypt или пусто
TRAEFIK_CERT_RESOLVER=

# Email для создания сертификатов в LetsEncrypt
TRAEFIK_CERTIFICATESRESOLVERS_LETSENCRYPT_ACME_EMAIL=???

# Обработчики (middlewares), возможные значения (можно комбинировать через запятую):
# basic-auth - включение basic-авторизации (пароль задается в TRAEFIK_BASIC_AUTH_USERS)
# redirect-to-non-www@file - редирект с www на без www (схема https)
# redirect-to-www@file - редирект на www (схема https)
# redirect-to-non-www-http@file - редирект с www на без www (схема http, то есть если не надо делать редирект сразу на https)
# redirect-to-www-http@file - редирект на www (схема http)
# redirect-to-https@file - редирект с http на https
# trim-index@file - обрезает index.php, index.html
TRAEFIK_SSL_MIDDLEWARES=basic-auth,redirect-to-non-www-http@file,trim-index@file
TRAEFIK_MIDDLEWARES=basic-auth,redirect-to-non-www-http@file,trim-index@file
# То же самое для Grafana, тут можно не закрывать basic-авторизацией, лучше создать сложный пароль в самой Grafana.
GRAFANA_SSL_MIDDLEWARES=redirect-to-non-www-http@file
GRAFANA_MIDDLEWARES=redirect-to-non-www-http@file

# Пользователи для basic-авторизации
# Для получения кредов выполнить: echo $(htpasswd -nb admin test)
# Важно: при указании пароля тут символы $ экранировать (вместо одного $ писать $$) то следует, то не следует, видимо это какие-то лаги трафика из релиза в релиз
TRAEFIK_BASIC_AUTH_USERS=admin:$$apr1$$vH.i1FEf$$V0VSouTyT6NuN2OE2KiI21

# Настройки Postgres, по умолчанию отключен
# Для включения добавить сверху в переменной COMPOSE_FILE строку
# :docker-compose/postgres.yml
# А если надо открыть порт (5432) то еще добавить
# :docker-compose/postgres.ports.yml
POSTGRES_VERSION=15
POSTGRES_USER=postgres
# Пароль пользователя Postgres - обязательно сменить на бою!!!
POSTGRES_PASSWORD=test
